from rest_framework import exceptions

class NotFound(exceptions.NotFound):
    default_detail = 'Не найдено.'
