from django.test import TestCase
from django.urls import reverse
import tempfile
import shutil
from PIL import Image
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from .models import Company
from rest_framework.test import APITestCase

class CompanyTests(APITestCase):

    def setUp(self):
        self.password = 'test'
        User.objects.create_user(username='user1', password=self.password)
        User.objects.create_user(username='user2', password=self.password)

        settings._original_media_root = settings.MEDIA_ROOT
        self._temp_media = tempfile.mkdtemp()
        settings.MEDIA_ROOT = self._temp_media

    def tearDown(self):
        shutil.rmtree(self._temp_media, ignore_errors=True)
        settings.MEDIA_ROOT = settings._original_media_root
        del settings._original_media_root

    def login_user(self, username, password=None):
        password = password or self.password
        x = self.client.login(username=username, password=password)
        return auth.get_user(self.client)

    def login_admin(self):
        return self.login_user(username='admin', password='admin')

    def create_company(self, status_code=201, **data):
        data.setdefault('name', 'test')
        data.setdefault('orn', '000000')
        res = self.client.post(reverse('companies-list'), data=data)
        self.assertEqual(res.status_code, status_code)
        return res.data

    def create_project(self, status_code=201, **data):
        data.setdefault('name', 'test')
        data.setdefault('status', 'ready')
        company_id = data.pop('company_id', self.create_company()['id'])
        res = self.client.post(reverse('projects-list', args=[company_id]), data=data)
        self.assertEqual(res.status_code, status_code)
        return res.data

    def create_gallery(self, status_code=201, **data):
        company_id = data.pop('company_id', self.create_company()['id'])
        project_id = data.pop('project_id', self.create_project(company_id=company_id)['id'])
        res = self.client.post(reverse('galleries-list', args=[company_id, project_id]), data=data)
        self.assertEqual(res.status_code, status_code)
        return res.data

    def create_image_file(self, name='test', ext='png'):
        from io import BytesIO
        temp_file = BytesIO()
        image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
        image.save(temp_file, 'png')
        temp_file.name = f'{name}.{ext}'
        temp_file.seek(0)
        return temp_file

    def create_image(self, status_code=201, **data):
        company_id = data.pop('company_id', self.create_company()['id'])
        project_id = data.pop('project_id', self.create_project(company_id=company_id)['id'])
        gallery_id = data.pop('gallery_id',
                              self.create_gallery(company_id=company_id, project_id=project_id)['id'])

        data.setdefault('image', self.create_image_file())
        res = self.client.post(reverse('gallery-images-list', args=[company_id, project_id, gallery_id]), data=data)
        self.assertEqual(res.status_code, status_code)
        return res.data

    def test_create_company(self):
        self.login_user('user1')
        company = self.create_company(name='new_name')
        self.assertEqual(company['name'], 'new_name')

    def test_create_gallery_image(self):
        self.login_user('user1')
        image = self.create_image(weight=0)
        self.assertEqual(image['weight'], 0)

    def test_company_list(self):
        Company.objects.create(name='test', orn='000000', user_id=1)
        Company.objects.create(name='test', orn='000000', user_id=1)
        res = self.client.get(reverse('companies-list'))
        self.assertEqual(len(res.data), 2)

    def test_company_item(self):
        # анониму запрещено создавать компании
        self.create_company(status_code=403)
        # залогиненый может создавать
        user = self.login_user('user1')
        company = self.create_company()
        # и обновлять
        res = self.client.patch(reverse('companies-detail', args=(company['id'], )), data={'name': 'new_name'})
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data['name'], 'new_name')
        # другой юзер не может править не свою компанию
        self.login_user('user2')
        res = self.client.patch(reverse('companies-detail', args=(company['id'],)), data={'name': 'new_name'})
        self.assertEqual(res.status_code, 403)
        # владелец может удалить свою компанию
        self.login_user('user1')
        res = self.client.delete(reverse('companies-detail', args=(company['id'],)))
        self.assertEqual(res.status_code, 204)
        companies_list = self.client.get(reverse('companies-list'))
        self.assertEqual(len(companies_list.data), 0)

        # админ может все
        self.login_user('user1')
        company = self.create_company()
        self.login_admin()
        res = self.client.patch(reverse('companies-detail', args=(company['id'],)), data={'name': 'new_name'})
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data['name'], 'new_name')
        res = self.client.delete(reverse('companies-detail', args=(company['id'],)))
        self.assertEqual(res.status_code, 204)
        companies_list = self.client.get(reverse('companies-list'))
        self.assertEqual(len(companies_list.data), 0)

    def test_gallery_images_list(self):
        # картинка в галарее создается
        self.login_user('user1')
        company = self.create_company()
        project = self.create_project(company_id=company['id'])
        gallery = self.create_gallery(company_id=company['id'], project_id=project['id'])
        image = self.create_image(company_id=company['id'], project_id=project['id'], gallery_id=gallery['id'])

        # неправильный урл возвращает 404 во всех случаях
        # если указан ид несуществующей компании
        res = self.client.get(reverse('gallery-images-list', args=(999, project['id'], gallery['id'])))
        self.assertEqual(res.status_code, 404)

        # или существующей, но той, которой этот проект не принаджежит
        company2 = self.create_company()
        res = self.client.get(reverse('gallery-images-list', args=(company2['id'], project['id'], gallery['id'])))
        self.assertEqual(res.status_code, 404)

        # если указан ид несуществующего проекта
        res = self.client.get(reverse('gallery-images-list', args=(company['id'], 999, gallery['id'])))
        self.assertEqual(res.status_code, 404)

        # или существующего, но того, которому эта галерея не принаджежит
        project2 = self.create_project()
        res = self.client.get(reverse('gallery-images-list', args=(company2['id'], project['id'], gallery['id'])))
        self.assertEqual(res.status_code, 404)

        # если указан ид несуществующей галлереи
        res = self.client.get(reverse('gallery-images-list', args=(company['id'], project['id'], 999)))
        self.assertEqual(res.status_code, 404)
        # или существующей, но той, которому это изображение не принаджежит
        gallery2 = self.create_gallery()
        res = self.client.get(reverse('gallery-images-list', args=(company['id'], project['id'], gallery2['id'])))
        self.assertEqual(res.status_code, 404)

    def test_galley_images_item(self):
        self.login_user('user1')
        # невозможно изменить файл при редактировании
        image = self.create_image(weight=0)
        pic = self.create_image_file(name='new_name')
        res = self.client.patch(reverse('gallery-images-detail', args=(1,1,1,1)), data={'image': pic, 'weight': 1})
        self.assertEqual(res.data['image'], image['image'])
        # а вес меняется
        self.assertEqual(res.data['weight'], 1)