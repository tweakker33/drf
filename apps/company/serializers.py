from rest_framework.serializers import Serializer, ModelSerializer
from rest_framework import serializers
from . import models


class Company(ModelSerializer):

    class Meta:
        model = models.Company
        fields = '__all__'
        read_only_fields = ('id', 'user')


class Project(ModelSerializer):

    class Meta:
        model = models.Project
        fields = '__all__'
        read_only_fields = ('id', 'company')

class ProjectGalleryImage(ModelSerializer):

    class Meta:
        model = models.ProjectGalleryImage
        fields = '__all__'
        read_only_fields = ('id', 'gallery')


class ProjectGalleryImageUpdate(ProjectGalleryImage):

    class Meta(ProjectGalleryImage.Meta):
        read_only_fields = ProjectGalleryImage.Meta.read_only_fields + ('image', )


class ProjectGallery(ModelSerializer):
    pictures = serializers.SerializerMethodField()

    class Meta:
        model = models.ProjectGallery
        fields = '__all__'
        read_only_fields = ('id', 'project')

    def get_pictures(self, obj):
        return ProjectGalleryImage(obj.images.order_by('weight'), many=True).data

class Tag(ModelSerializer):

    class Meta:
        model = models.Tag
        fields = '__all__'

