from django.shortcuts import render
from rest_framework import viewsets, mixins, permissions as rest_permissions, request
from . import serializers, permissions
from .models import Company, Project, ProjectGallery, ProjectGalleryImage, Tag
from .exceptions import NotFound
# Create your views here.


class BaseViewSet(viewsets.ModelViewSet):

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)
        self.custom_validate(request, *args, **kwargs)

    def custom_validate(self, request, *args, **kwargs):
        # выполняем все cusom_validate_ методы
        checker = lambda x: (x.startswith('custom_validate_') and callable(getattr(self, x)))
        for item in filter(checker, dir(self)):
            getattr(self, item)(request, *args, **kwargs)


def company_url_validate(request, *args, **kwargs):
    company_id = int(kwargs.pop('company_id'))
    try:
        return Company.objects.get(pk=company_id)
    except Company.DoesNotExist:
        raise NotFound('Комапния с указанным id не существует.')


def project_url_validate(request, *args, **kwargs):
    company_id = int(kwargs.pop('company_id'))
    project_id = int(kwargs.pop('project_id'))
    try:
        project = Project.objects.filter(pk=project_id).get()
    except Project.DoesNotExist:
        raise NotFound('Проект с указанным id не существует.')
    if project.company_id != company_id:
        raise NotFound
    return project


def gallery_url_validate(request, *args, **kwargs):
    project_id = int(kwargs.pop('project_id'))
    gallery_id = int(kwargs.pop('gallery_id'))
    try:
        gallery = ProjectGallery.objects.get(pk=gallery_id)
    except ProjectGallery.DoesNotExist:
        raise NotFound('Галерея с указанным id не существует.')
    if project_id != gallery.project_id:
        raise NotFound


class CompanyViewSet(BaseViewSet):
    queryset = Company.objects.all()
    serializer_class = serializers.Company
    permission_classes = [permissions.StaffOrOwner, ]

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)


class ProjectViewSet(BaseViewSet):
    serializer_class = serializers.Project
    permission_classes = [permissions.StaffOrOwner, ]

    def get_queryset(self):
        return Project.objects.filter(company_id=self.kwargs['company_id'])

    def perform_create(self, serializer):
        serializer.save(company_id=self.kwargs['company_id'])

    def custom_validate_url(self, request, *args, **kwargs):
        company_url_validate(request, *args, **kwargs)


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = serializers.Tag
    permission_classes = [permissions.StaffOrReadOnly, ]


class ProjectGalleryViewSet(BaseViewSet):
    serializer_class = serializers.ProjectGallery
    permission_classes = [permissions.StaffOrOwner, ]

    def get_queryset(self):
        project_id = int(self.kwargs['project_id'])
        return ProjectGallery.objects.filter(project_id=project_id)

    def perform_create(self, serializer):
        serializer.save(project_id=self.kwargs['project_id'])

    def custom_validate_url(self, request, *args, **kwargs):
        company_url_validate(request, *args, **kwargs)
        project_url_validate(request, *args, **kwargs)


class ProjectGalleryImageViewSet(BaseViewSet):
    serializer_class = serializers.ProjectGalleryImage
    permission_classes = [permissions.StaffOrOwner, ]

    def get_queryset(self):
        gallery_id = self.kwargs['gallery_id']
        return ProjectGalleryImage.objects.filter(gallery_id=gallery_id).order_by('weight')

    def get_serializer_class(self):
        # вносить правки можно во все поля кроме файла
        if self.request.method in {'PUT', 'PATCH'}:
            return serializers.ProjectGalleryImageUpdate
        return serializers.ProjectGalleryImage

    def perform_create(self, serializer):
        serializer.save(gallery_id=self.kwargs['gallery_id'])

    def custom_validate_url(self, request, *args, **kwargs):
        company_url_validate(request, *args, **kwargs)
        project_url_validate(request, *args, **kwargs)
        gallery_url_validate(request, *args, **kwargs)
