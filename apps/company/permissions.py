from rest_framework import permissions


class BaseViewSetPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        user = request.user
        # админ может все
        if getattr(user, 'is_staff', False):
            return True
        if request.method in permissions.SAFE_METHODS:
            return True
        return getattr(self, 'post', lambda *x: False)(request, view)

    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_staff:
            return True
        if request.method in permissions.SAFE_METHODS:
            return True
        return getattr(self, request.method.lower(), lambda *x: False)(request, view, obj)


class StaffOrOwner(BaseViewSetPermission):

    def put(self, request, view, obj):
        # только владелец может обновлять
        return request.user.id == obj.user.id

    def patch(self, request, view, obj):
        return self.put(request, view, obj)

    def delete(self, request, view, obj):
        return request.user.id == obj.user.id

    def post(self, request, view):
        return request.user.is_authenticated


class StaffOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        return (request.user.is_staff or request.method in permissions.SAFE_METHODS)
