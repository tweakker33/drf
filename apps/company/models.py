from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Company(models.Model):
    user = models.ForeignKey(User, related_name='companies', on_delete=models.SET(1))
    name = models.CharField(max_length=255)
    orn = models.CharField(max_length=16)


class Project(models.Model):
    PROJECT_STATUSES = (
        ('project', 'В стадии проекта'),
        ('development', 'В разработке'),
        ('ready', 'Готов'),
    )

    company = models.ForeignKey(Company, related_name='projects', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    status = models.CharField(choices=PROJECT_STATUSES, max_length=32, null=True, blank=True)
    tags = models.ManyToManyField('Tag', blank=True)

    @property
    def user(self):
        return self.company.user

class ProjectGallery(models.Model):
    project = models.ForeignKey(Project, related_name='gallery', on_delete=models.CASCADE)

    @property
    def user(self):
        return self.project.user


def project_gallery_images_path(instance, filename):
    ROOT = 'project/galleries/'
    return f'{ROOT}{instance.gallery_id}/{filename}'


class ProjectGalleryImage(models.Model):

    gallery = models.ForeignKey(ProjectGallery, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(upload_to=project_gallery_images_path)
    weight = models.IntegerField(null=True, default=0)

    @property
    def user(self):
        return self.gallery.user


class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name