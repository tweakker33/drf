"""base URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from base.routers import NestedDefaultRouter
from rest_framework.routers import DefaultRouter
from apps.company import views
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='WIN API')


router = NestedDefaultRouter()
company_router = router.register('companies', views.CompanyViewSet, basename='companies')
project_router = company_router.register('projects', views.ProjectViewSet, base_name='projects', parents_query_lookups=['company_id'])
gallery_router = project_router.register('gallery', views.ProjectGalleryViewSet, base_name='galleries',
                        parents_query_lookups=['company_id', 'project_id'])
gallery_router.register('images', views.ProjectGalleryImageViewSet, base_name='gallery-images',
                        parents_query_lookups=['company_id', 'project_id', 'gallery_id'])
router.register('tags', views.TagViewSet, basename='tags')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(router.urls)),
    path('api/docs/', schema_view)
]
