from rest_framework.routers import DefaultRouter
from rest_framework_extensions.routers import NestedRouterMixin, NestedRegistryItem


class NestedRegistryItemCustom(NestedRegistryItem):

    def register(self, prefix, viewset, base_name, parents_query_lookups):
        self.router._register(
            prefix=self.get_prefix(current_prefix=prefix, parents_query_lookups=parents_query_lookups),
            viewset=viewset,
            base_name=base_name,
        )
        return self.__class__(
            router=self.router,
            parent_prefix=prefix,
            parent_item=self,
            parent_viewset=viewset
        )

    def get_parent_prefix(self, parents_query_lookups):
        prefix = '/'
        current_item = self
        i = len(parents_query_lookups) - 1
        while current_item:
            parent_lookup_value_regex = getattr(current_item.parent_viewset, 'lookup_value_regex', '[^/.]+')
            prefix = '{parent_prefix}/(?P<{parent_pk_kwarg_name}>{parent_lookup_value_regex})/{prefix}'.format(
                parent_prefix=current_item.parent_prefix,
                parent_pk_kwarg_name=parents_query_lookups[i],
                parent_lookup_value_regex=parent_lookup_value_regex,
                prefix=prefix
            )
            i -= 1
            current_item = current_item.parent_item
        return prefix.strip('/')


class NestedRouterMixinCustom:
    def _register(self, *args, **kwargs):
        return super().register(*args, **kwargs)

    def register(self, *args, **kwargs):
        self._register(*args, **kwargs)
        return NestedRegistryItemCustom(
            router=self,
            parent_prefix=self.registry[-1][0],
            parent_viewset=self.registry[-1][1]
        )

class NestedDefaultRouter(NestedRouterMixinCustom, DefaultRouter):
    pass